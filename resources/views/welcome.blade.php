<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous" style="margin:10px;">

 <title>Constancia y inscripcion</title>
</head>
<body>

<div class="container">
<div class="card text-center">
  <div class="card-header">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Coat_of_arms_of_Quintana_Roo.svg/1200px-Coat_of_arms_of_Quintana_Roo.svg.png" alt="..." class="img-thumbnail" width="50" height="50">
  Secretaría de Gobierno  
Dirección General del Registro Público de la Propiedad y del Comercio del Estado de Quintana Roo. 
GOB EDO 
QUINTANA ROO 
OFICINA REGISTRAL:CHETUMAL  
  </div>
  <div >
  <h4>CONSTANCIA DE INSCRIPCION</h4>
  </div>
  <div class="card-body">
    <h5 class="card-title">HACE CONSTAR </h5>
    <p class="card-text">QUE EN EL FOLIO 309714 DE ESTA OFICINA,SE ENCUENTRA INSCRITO EL ACTO CONSTANCIA DE INSCRIPCIÓN BAJO EL NUMERO 1 Y DE FECHA --/--/----</p>
    <h6 align="left">COMO TITULARES ACTUALES: </h6>
<h6 align="left">RESPECTO AL PREDIO DENOMINADO:</h6>
    <h5 class="card-title">ATENTAMENTE </h5>
    <p class="card-text">A PETICION DE LA PARTE INTERESADA Y PARA LOS USOS LEGALES QUE CORRESPONDAN SE EXTIENDE LA PRESENTE CONSTANCIA DE INSCRIPCION EN LA CIUDAD DE CHETUMAL.</p>
<h6>"SUFRAGIO EFECTIVO NO REELECCIÓN"</h6>
<h6>QUINTANA ROO SIENDO LAS 05:37 PM HORAS DEL DÍA 20 DE AGOSTO DEL 2020 .</h6>
<h6 align="left">Reviso y Elaboró.</h6>
  <h6 align="left">VERONICA BARAJAS ROJAS </h6>
  <h6 align="left">DELEGADA DEL REGISTRO PÚBLICO </h6>
  <h6 align="left">ANALISTA  DE LA PROPIEDAD Y DEL COMERCIO EN BENITO JUAREZ</h6>
  <h6 align="left">RESPECTO AL PREDIO DENOMINADO:</h6>
  <h6 align="left">RESPECTO AL PREDIO DENOMINADO:</h6>
  </div>
 
  <div class="card-footer text-muted">
    todos los derechos reservados Gobierno de chetumal@2020
  </div>
</div>
  </div>
</body>
</html>
